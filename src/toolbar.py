from PyQt6.QtGui import *
from PyQt6.QtWidgets import *
from PyQt6.QtCore import *
from toolbox import get_spacer_item


START_ICON = QIcon('icon/start.svg')
PAUSE_ICON = QIcon('icon/pause.svg')
REFRESH_ICON = QIcon('icon/refresh.svg')

SPACING = 6

class ToolBar(QWidget):
    def __init__(self, parent) -> None:
        super().__init__(parent)

        container = QWidget()
        container.setObjectName("ToolBar")
        layout = QHBoxLayout()
        
        self.play_button = QPushButton(START_ICON ,"开始模拟")
        self.play_button.setFlat(True)
        layout.addWidget(self.play_button)
        layout.addSpacing(SPACING)
        
        self.restart_button = QPushButton(REFRESH_ICON, "重新模拟")
        self.restart_button.setFlat(True)
        layout.addWidget(self.restart_button)
        layout.addSpacing(SPACING)
        
        label = QLabel("调度策略")
        layout.addWidget(label)
        dispatching_comboBox = QComboBox()
        dispatching_comboBox.setFixedWidth(128)
        self.dispatching_comboBox = dispatching_comboBox
        layout.addWidget(self.dispatching_comboBox)
        layout.addSpacing(SPACING)
        
        label = QLabel("模拟速度")
        layout.addWidget(label)
        speed_slider = QSlider()
        speed_slider.setOrientation(Qt.Orientation.Horizontal)
        speed_slider.setTickInterval(1)
        speed_slider.setMinimum(1)
        speed_slider.setMaximum(20)
        speed_slider.setFixedWidth(128)
        self.speed_slider = speed_slider

        layout.addWidget(speed_slider)
        layout.addSpacerItem(get_spacer_item(expand_v = False))

        layout.setSpacing(2)
        layout.setContentsMargins(2, 2, 2, 2)
        container.setLayout(layout)

        layout = QHBoxLayout()
        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(container)

        self.setLayout(layout)

    def set_play_button_state(self, is_play:bool):
        if is_play:
            self.play_button.setIcon(START_ICON)
            self.play_button.setText("开始模拟")
        else:
            self.play_button.setIcon(PAUSE_ICON)
            self.play_button.setText("暂停模拟")