from PyQt6.QtGui import *
from PyQt6.QtWidgets import *
from PyQt6.QtCore import *

class MenuBar(QWidget):
    def __init__(self, parent) -> None:
        super().__init__(parent)

        menu_bar = QMenuBar()
        file_menu = menu_bar.addMenu("文件")

        self.open_vehicle_conf_action = QAction("打开车辆配置文件", file_menu)
        self.open_flight_data_file_action = QAction("打开航班信息文件", file_menu)
        self.open_gate_data_file_action = QAction("打开停机位信息文件", file_menu)

        file_menu.addAction(self.open_gate_data_file_action)
        file_menu.addAction(self.open_flight_data_file_action)
        file_menu.addAction(self.open_vehicle_conf_action)
        file_menu.addSeparator()

        self.save_chart_action = QAction("保存曲线图", file_menu) 
        self.save_dipath_plan_action = QAction("保存调度方案", file_menu)      
        
        file_menu.addAction(self.save_chart_action)
        file_menu.addAction(self.save_dipath_plan_action)
        file_menu.addSeparator()

        self.close_app_action = QAction("退出", file_menu)
        self.close_app_action.triggered.connect(self.window().close)
        file_menu.addAction(self.close_app_action)

        view_menu = menu_bar.addMenu("查看")
        self.chart_view_action = QAction("图表", view_menu)
        self.chart_view_action.setCheckable(True)

        self.graph_view_action = QAction("图示", view_menu)
        self.graph_view_action.setCheckable(True)
        self.graph_view_action.setChecked(True)

        action_group = QActionGroup(view_menu)
        action_group.addAction(self.chart_view_action)
        action_group.addAction(self.graph_view_action)
        action_group.setExclusive(True)

        view_menu.addAction(self.chart_view_action)
        view_menu.addAction(self.graph_view_action)

        view_menu.addSeparator()

        self.show_info_action = QAction("信息部件", view_menu)
        self.show_info_action.setCheckable(True)
        self.show_info_action.setChecked(True)
        view_menu.addAction(self.show_info_action)

        dispatch_menu = menu_bar.addMenu("算法")
        self.show_cost_figure_action = QAction("查看代价图")
        dispatch_menu.addAction(self.show_cost_figure_action)

        self.ga_parms_action = QAction("遗传算法参数调整")
        dispatch_menu.addAction(self.ga_parms_action)

        layout = QHBoxLayout()
        layout.addWidget(menu_bar)
        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)
    
