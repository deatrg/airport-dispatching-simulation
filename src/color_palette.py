from PyQt6.QtGui import QColor
# 配色地址 https://flatuicolors.com/palette/defo

DONT_NEED_VEHICLE_COLOR = QColor("#d35400")
NO_VEHICLE_COLOR = QColor("#2c3e50")
VEHICLE_COMING_COLOR = QColor("#2980b9")
VEHICLE_WAITING_COLOR = QColor("#8e44ad")
VEHICLE_COMPLETE_COLOR = QColor("#27ae60")
VEHICLE_UNCOMPLETE_COLOR = QColor("#bdc3c7")

SCENE_BG_COLOR = QColor("#fafafa")
ROAD_COLOR = QColor("#7f8c8d")
VEHICLE_ITEM_COLOR = QColor("#9b59b6")
PARK_ITEM_COLOR = QColor("#3498db")
GATE_ITEM_COLOR = QColor("#1abc9c")