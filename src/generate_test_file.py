from random import randint, sample
from agh_obj import VehicleType, Clock
import json
import csv

GATE_NAME_SEED = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
MIN = 60

is_generate_map = True
is_generate_flight = False
is_generate_vehicle = False

if is_generate_map:
    park_points = [(0.0, 0.0)]
    gate_points = []
    points = [(0.0, 0.0)]
    edges = []

    def gan(start, end, scale, k, symbol):
        for i in range(start, end):
            x = i * scale * symbol - 50 * symbol
            y = x * k * symbol
            gate_points.append((x, y + 100.0))
            gate_points.append((x, -y - 100.0))

            points.append((x, y))
            points.append((x, -y))
            points.append((x, y + 100.0))
            points.append((x, -y - 100.0))

        x = end * scale * symbol
        y = x * k * symbol
        gate_points.append((x, y))
        gate_points.append((x, -y))

        points.append((x, y))
        points.append((x, -y))
        points.append((start * scale * symbol - 50 * symbol, 0))

    gan(2, 6, 100, 0.5, -1)
    gan(2, 7, 100, 0.25, 1)

    for start, end in [(1, 17), (20, 40)]:
        for i in range(start, end, 4):
            edges.append((i, i + 2))
            edges.append((i, i + 4))
            edges.append((i + 1, i + 3))
            edges.append((i + 1, i + 5))
        edges.append((start, end + 2))
        edges.append((start + 1, end + 2))
        edges.append((end + 2, 0))
        
    data = {}
    data["points"] = points
    data["edges"] = edges
    data["gates"] = []
    data["parks"] = []

    near_gate = (3, 4, 22, 23, 26, 27)

    for i, point in enumerate(gate_points):
        tmp = {}
        point_index = points.index(point)
        tmp["index"] = point_index
        tmp["name"] = GATE_NAME_SEED[i]
        
        if point_index in near_gate:
            tmp["is_near_gate" ] = True
        else:
            tmp["is_near_gate"] = False

        data["gates"].append(tmp)

    for i, point in enumerate(park_points):
        tmp = {}
        tmp["index"] = points.index(point)
        tmp["name"] = "Park-" + GATE_NAME_SEED[i]
        
        vehicles = []
        for name in VehicleType.__members__.keys():
            dic = {}
            dic["type"] = name
            dic["num"] = randint(8, 12)
            vehicles.append(dic)
        tmp["vehicles"] = vehicles
        data["parks"].append(tmp)


    with open('test_map.json', 'w') as f:
        json.dump(data, f)

if is_generate_flight:
    def generate_test_flight_data(gate_names):
        tmp = []
        check = {}
        for i in range(8, 18):
            for g in sample(gate_names, 13):
                if randint(0, 1) % 2 == 0:
                    time = "2021-05-20 {:0>2d}:{:0>2d}".format(i, randint(0, 11)*5)
                    if g in check and Clock.get_time_stamp(time) - Clock.get_time_stamp(check[g]) < 55 * 60:
                        continue
                    tmp.append((g+time[-5:].replace(":", ''), time, g))
                    check[g] = time
        tmp.sort(key=lambda x: x[1])
        return tmp
    
    flight = generate_test_flight_data(GATE_NAME_SEED[:22])

    with open("test_flight_1.csv", 'w', newline='') as f:
        csv_writer = csv.writer(f)
        csv_writer.writerows(flight)

if is_generate_vehicle:
    def generate_test_vehicle_data():
        vehicle_data = []
        
        for name in VehicleType.__members__ .keys():
            dic = {}
            dic["type"] = name
            dic["speed"] = randint(1, 3)
            dic["capcity"] = randint(1, 2)
            dic["service_time_cost"] = randint(1, 3) * 4 * MIN
            dic["cost"] = 20 + randint(1, 5) * 5
            dic["recover_time_cost"] = randint(5, 10) * MIN

            vehicle_data.append(dic)
        return vehicle_data
    data = generate_test_vehicle_data()
    with open('test_vehicle.json', 'w') as f:
        json.dump(data, f)
