### 机场调度模拟仿真系统

#### 介绍
针对地勤车辆调度问题，构建了带有服务次数限制及时间窗的7种地勤车型车辆的联合调度仿真模型，并在该模型上实现了基于PyQt平台的可视化机场地勤车辆调度模拟仿真系统，能将建模优化后的地勤车辆调度方案以更为直观的、图形化的方式展现出来，以便能更好地去了解、对比不同的调度方案所带来的不同效果。并以最小化地勤车辆行驶费用代价为目标，用遗传算法优化了地勤车辆调度过程，且与先来先服务算法进行了对比分析。

#### 软件架构
程序使用Python语言进行开发，基于PyQt6实现了图形界面


#### 使用步骤

1.  安装 Python>= 3.9
2.  安装依赖

```
pip install PyQt6 PyQt6-Charts
```
3.  从src文件夹打开命令行执行如下命令

```
python main.py
```


#### 打包说明
推荐使用[Nuitka](https://github.com/Nuitka/Nuitka)进行打包，打包命令如下：

```
nuitka --standalone --enable-plugin=pyqt6 --include-data-dir=.\src\icon=icon --include-data-file=.\src\style.qss=style.qss --windows-disable-console --windows-icon-from-ico=.\src\icon\app_icon.ico .\src\main.py
```

#### 软件截图
![输入图片说明](screenshoot/screen_shoot_1.png)
![输入图片说明](screenshoot/screen_shoot_2.png)


